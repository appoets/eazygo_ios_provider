//
//  StatusRideEntity.swift
//  GoJekProvider
//
//  Created by Sudar vizhi on 12/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct StatusRideEntity : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [String]?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}
